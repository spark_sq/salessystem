package com.siki.consumer.controller;

import com.siki.salessystemcommon.utils.SystemMsgJsonResponse;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@Api(tags = "[测试接口]")
@RequestMapping("/api")
@RequiredArgsConstructor
public class TestController {

    private final String PROVIDER_URL="http://xxx.com";
    private final RestTemplate template;

    @GetMapping("/current")
    public SystemMsgJsonResponse get(Authentication authentication){
        return SystemMsgJsonResponse.success(template
                .getForObject(PROVIDER_URL+"/get",Object.class));
    }
}
