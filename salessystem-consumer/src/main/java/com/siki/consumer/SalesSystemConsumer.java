package com.siki.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

@SpringBootApplication
@EnableDiscoveryClient
// spring security注解，代表使用表达式时间方法级别的安全性 4个注解可用
@EnableGlobalMethodSecurity(prePostEnabled = true)
@ComponentScan(basePackages = {"com.siki.salessystemcommon","com.siki.consumer"})
public class SalesSystemConsumer {
    public static void main(String[] args) {
        SpringApplication.run(SalesSystemConsumer.class,args);
    }
}
