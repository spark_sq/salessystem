package com.siki.salessystemcommon.utils;

import com.alibaba.fastjson.JSONObject;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

@Component
public class EnumUtil {
    /**
     * @param clazz 枚举类型
     * @return
     */
    @SneakyThrows
    public static JSONObject getEnumTypeJsonArray(Class<? extends Enum> clazz) {
        JSONObject jsonObject = new JSONObject();
        Enum[] es = clazz.getEnumConstants();
        Method getName = clazz.getMethod("getName");
        for (Enum e : es) {
            String chineseName = getName.invoke(e).toString();
            jsonObject.put(e.name(), chineseName);
        }
        return jsonObject;
    }
}
