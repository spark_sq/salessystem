package com.siki.salessystemcommon.utils;

import java.util.List;
import java.util.Set;

import com.siki.salessystemcommon.dto.CurrentUser;
import org.springframework.security.core.context.SecurityContextHolder;

import com.alibaba.fastjson.JSON;



public class SecurityUtil {


    public static CurrentUser currentUser() {
        CurrentUser currentUser = JSON.parseObject(JSON
                .parseObject(JSON.toJSONString(SecurityContextHolder.getContext().getAuthentication()))
                .getJSONObject("userAuthentication").getJSONObject("details").getJSONObject("principal").toJSONString(),
                CurrentUser.class);
        return currentUser;
    }

    /**
     * 用于获得当前用户的角色id
     * @return
     */
    public static List < Integer > getRoleIds() {
        return currentUser().getRoleIds();
    }

    /**
     * 用于获得当前权限列表
     *
     */
    public static Set <CurrentUser.Authority> getGrantedAuthoritys() {
        return currentUser().getAuthorities();
    }

    /**
     * 是否含有该权限
     *
     * @param menu
     */
    public static boolean isGrantedAuthority(String menu) {
        return currentUser().getAuthorities().contains(menu);
    }

    /**
     * 用于获得当前用户属性
     *
     * @param key
     */
    public static Object getParam(String key) {
        return currentUser().getParams().get(key);
    }
}
