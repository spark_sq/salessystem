package com.siki.salessystemcommon.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.siki.salessystemcommon.entity.base.BaseUuEntity;
import com.siki.salessystemcommon.entity.enumeration.SexEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;
/**
 * @Author Siki
 * @Date 2020/12/1
 */
@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
@TableName(value = "user",resultMap = "userResultMap")
public class User extends BaseUuEntity {
    @ApiModelProperty("账号")
    private String account;

    @ApiModelProperty("密码")
    private String password;

    @ApiModelProperty("真实姓名")
    private String realName;

    @ApiModelProperty("联系方式")
    private String phone;

    /**
     * 枚举
     */
    @ApiModelProperty("性别")
    private SexEnum userSex;
}
