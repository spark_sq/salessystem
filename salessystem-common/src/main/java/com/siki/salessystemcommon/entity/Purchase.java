package com.siki.salessystemcommon.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siki.salessystemcommon.entity.base.BaseAutoEntity;


import com.siki.salessystemcommon.entity.enumeration.PurchaseStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * @Author Siki
 * @Date 2020/12/10
 */

@ApiModel("购物清单类")
@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
@TableName(value = "purchase")
public class Purchase extends BaseAutoEntity {

    @ApiModelProperty("清单状态")
    private PurchaseStatusEnum purchaseStatus;

    @ApiModelProperty("起始地")
    private String startingPoint;

    @ApiModelProperty("收货地")
    private String endingPoint;

    @ApiModelProperty("合同id")
    private Long contractId;

    @ApiModelProperty("采购清单总金额")
    private String purchaseTotalMoney;

    @ApiModelProperty("采购清单已付金额")
    private String purchasePaidMoney;
}
