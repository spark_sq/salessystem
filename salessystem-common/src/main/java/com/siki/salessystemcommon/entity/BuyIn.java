package com.siki.salessystemcommon.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siki.salessystemcommon.entity.base.BaseAutoEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
/**
 * @author Siki
 * @date 2020/12/29
 */
@ApiModel("进货单实体")
@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
@TableName("buy_in")
public class BuyIn extends BaseAutoEntity {
    @ApiModelProperty("商品id")
    private Long commodityId;

    @ApiModelProperty("商品名称")
    private String commodityName;

    @ApiModelProperty("商品数量")
    private Integer commodityAmount;

    @ApiModelProperty("仓库管理员id")
    private String userId;
}
