package com.siki.salessystemcommon.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.siki.salessystemcommon.entity.base.BaseUuEntity;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;


/**
 * @Author Siki
 * @Date 2020/12/6
 */

@ApiModel("客户实体")
@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
@TableName(value = "client", resultMap = "com.siki.salessystemcommon.mapper.SalesMapper.salesResultMap")
public class Client extends BaseUuEntity {


}
