package com.siki.salessystemcommon.entity;


import com.baomidou.mybatisplus.annotation.TableName;
import com.siki.salessystemcommon.entity.base.BaseAutoEntity;
import com.siki.salessystemcommon.entity.base.BaseEntity;
import com.siki.salessystemcommon.entity.enumeration.ContractStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;


/**
 * @Author Siki
 * @Date 2020/12/6
 */
@ApiModel("合同实体类")
@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
@TableName(value = "contract")
public class Contract extends BaseAutoEntity {
    @ApiModelProperty("合同编号")
    private String contractName;

    @ApiModelProperty("合同描述")
    private String contractDescription;

    @ApiModelProperty("合同状态")
    private ContractStatusEnum contractStatus;

    @ApiModelProperty("合同总金额")
    private String contractTotalMoney;

    @ApiModelProperty("合同已支付金额")
    private String contractPaidMoney;

}
