package com.siki.salessystemcommon.entity.enumeration;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.Getter;

@Getter
public enum InvoiceStatusEnum {
    NO_PAID("未付款"),
    PAID("已付款"),
    SHIPPED("已发货"),
    NOT_SHIPPED("未发货"),
    END("完结");

    @EnumValue
    private final String name;

    InvoiceStatusEnum(String name) {
        this.name = name;
    }
}
