package com.siki.salessystemcommon.entity;


import com.baomidou.mybatisplus.annotation.TableName;
import com.siki.salessystemcommon.entity.base.BaseAutoEntity;
import com.siki.salessystemcommon.entity.enumeration.InvoiceStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
/**
 * @Author Siki
 * @Date 2020/12/15
 */
@ApiModel("发货单实体")
@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
@TableName(value = "invoice",autoResultMap = true)
public class Invoice extends BaseAutoEntity {
    @ApiModelProperty("购物清单id")
    private Long purchaseId;

    @ApiModelProperty("商品id")
    private Long commodityId;

    @ApiModelProperty("商品数量")
    private Integer commodityAmount;

    @ApiModelProperty("发货单状态")
    private InvoiceStatusEnum invoiceStatus;
}
