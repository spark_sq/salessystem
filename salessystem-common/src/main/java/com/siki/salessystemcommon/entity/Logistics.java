package com.siki.salessystemcommon.entity;


import com.baomidou.mybatisplus.annotation.TableName;
import com.siki.salessystemcommon.entity.base.BaseAutoEntity;
import com.siki.salessystemcommon.entity.enumeration.LogisticsStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
/**
 * @Author Siki
 * @Date 2020/12/17
 */
@ApiModel("物流实体类")
@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
@TableName(value = "logistics")
public class Logistics extends BaseAutoEntity {
    @ApiModelProperty("物流单编号")
    private String logisticsName;

    @ApiModelProperty("发货地")
    private String startingPoint;

    @ApiModelProperty("收货地")
    private String endingPoint;

    @ApiModelProperty("出货单id")
    private Long invoiceId;

    @ApiModelProperty("物流状态")
    private LogisticsStatusEnum logisticsStatus;
}
