package com.siki.salessystemcommon.entity.enumeration;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.Getter;

@Getter
public enum LogisticsStatusEnum {

    NOT_RECEIVED("未签收"),
    RECEIVER("已签收");

    @EnumValue
    String name;

    LogisticsStatusEnum(String name) {
        this.name = name;
    }
}
