package com.siki.salessystemcommon.entity;

import com.siki.salessystemcommon.entity.base.BaseAutoEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @Author Siki
 * @Date 2020/12/1
 */

@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel("权限实体")
public class Permission extends BaseAutoEntity {

    @ApiModelProperty("权限名称")
    String permissionName;

    @ApiModelProperty("权限")
    List<Role> roles;
}
