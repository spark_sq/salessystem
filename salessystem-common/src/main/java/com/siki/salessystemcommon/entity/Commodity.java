package com.siki.salessystemcommon.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.siki.salessystemcommon.entity.base.BaseAutoEntity;
import com.siki.salessystemcommon.entity.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
/**
 * @author Siki
 * @date 2020/12/28
 */
@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel("商品实体")
@TableName(value = "commodity",resultMap = "com.siki.salessystemcommon.mapper.CommodityMapper.commodityResultMap")
public class Commodity extends BaseAutoEntity {
    @ApiModelProperty("商品名称")
    private String commodityName;

    @ApiModelProperty("商品价格")
    private String commodityPrice;

    @ApiModelProperty("商品库存")
    private Integer commodityInventory;

    @ApiModelProperty("商品图片")
    private String commodityImage;
}
