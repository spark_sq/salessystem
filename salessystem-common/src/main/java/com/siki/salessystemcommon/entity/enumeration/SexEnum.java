package com.siki.salessystemcommon.entity.enumeration;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.Getter;

@Getter
public enum SexEnum {
    MALE("男"),
    FEMALE("女");

    SexEnum(String name) {
        this.name = name;
    }

    @EnumValue
    private final String name;

}
