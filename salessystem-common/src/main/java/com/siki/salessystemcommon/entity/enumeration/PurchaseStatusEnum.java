package com.siki.salessystemcommon.entity.enumeration;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.Getter;

@Getter
public enum PurchaseStatusEnum {
    UNPAID("未支付"),
    PAID("已支付"),
    END("已完成");

    @EnumValue
    String name;

    PurchaseStatusEnum(String name) {
        this.name = name;
    }
}
