package com.siki.salessystemcommon.entity;

import com.siki.salessystemcommon.entity.base.BaseAutoEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @Author Siki
 * @Date 2020/12/1
 */
@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
public class Role extends BaseAutoEntity {

    @ApiModelProperty("身份名称")
    String roleName;

    @ApiModelProperty("用户")
    List<User> users;

    @ApiModelProperty("权限")
    List<Permission> permissions;
}
