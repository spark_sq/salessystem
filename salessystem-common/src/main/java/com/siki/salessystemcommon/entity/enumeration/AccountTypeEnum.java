package com.siki.salessystemcommon.entity.enumeration;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.Getter;

@Getter
public enum AccountTypeEnum {
    // 1
    SALES_ADMIN("销售管理员"),
    // 2
    SALES("销售人员"),
    // 3
    CLIENT("客户"),
    // 4
    DEPOT_ADMIN("仓库管理员"),
    // 5
    PRO_ADMIN("超级管理员");

    @EnumValue
    String name;

    AccountTypeEnum(String name) {
        this.name = name;
    }
}
