package com.siki.salessystemcommon.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.siki.salessystemcommon.entity.base.BaseUuEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @Author Siki
 * @Date 2020/12/6
 */

@ApiModel("销售人员实体")
@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
@TableName(value = "sales",resultMap = "com.siki.salessystemcommon.mapper.SalesMapper.salesResultMap")
public class Sales extends BaseUuEntity {
    @ApiModelProperty("绩效")
    String performance;
}
