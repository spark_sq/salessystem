package com.siki.salessystemcommon.entity.enumeration;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.Getter;
/**
 * @author Siki
 * @date 2020/12/28
 */
@Getter
public enum ContractStatusEnum {
    // 签订中
    SIGN("签订中"),
    // 履行中
    FULFILL("履行中"),
    // 结束
    OVER("完结");

    @EnumValue
    private final String name;

    ContractStatusEnum(String name) {
        this.name = name;
    }
}
