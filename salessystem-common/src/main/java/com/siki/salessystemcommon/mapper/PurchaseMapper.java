package com.siki.salessystemcommon.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.siki.salessystemcommon.entity.Purchase;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface PurchaseMapper extends BaseMapper<Purchase> {
    /**
     * 保存采购清单与商品关系
     * @param map 数据整合
     */
    void insertPurchaseCommodity(Map<String,Object> map);

    /**
     * 删除采购清单和商品之间的关系
     * @param id 采购清单id
     */
    void deletePurchaseAndCommodity(Long id);

    /**
     * 更改清单中商品的出货状态
     * @param purchaseId 采购清单id
     * @param map 采购清单与商品的数据
     */
    void updatePurchaseAndCommodity(Long purchaseId, Map<String ,Object> map);

    /**
     * 插入清单与商品的关系
     * @param purchaseId 采购清单id
     * @param map 采购清单与商品的数据
     */
    void insertPurchaseAndCommodity(Long purchaseId, Map<String ,Object> map);
}
