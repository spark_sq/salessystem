package com.siki.salessystemcommon.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.siki.salessystemcommon.entity.Commodity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface CommodityMapper extends BaseMapper<Commodity> {
    /**
     * 根据合同id 查询对应的商品列表
     *
     * @param id 合同id
     * @return 商品列表
     */
    List<Commodity> getCommodityByContractId(Long id);

    /**
     * 根据合同id和商品id查询数量
     *
     * @param contractId  合同id
     * @param commodityId 商品id
     * @return 商品数量
     */
    Map<String, Integer> getContractCommodityAmount(Long contractId, Long commodityId);

    /**
     * 更新合同对应商品的未发货数量
     *
     * @param contractId            合同id
     * @param commodityId           商品id
     * @param commodityNoSendAmount 商品未发货数量(需要先计算完毕)
     */
    void updateContractNoSeedCommodityAmount(Long contractId, Long commodityId, Integer commodityNoSendAmount);

    /**
     * 根据清单id查询绑定的商品信息
     * @param purchaseId 采购清单id
     */
    List<Map<String,Object>> getPurchaseCommodityAmount(Long purchaseId);
}
