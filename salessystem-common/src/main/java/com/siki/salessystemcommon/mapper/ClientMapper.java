package com.siki.salessystemcommon.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.siki.salessystemcommon.entity.Client;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @Author Siki
 * @Date 2020/12/6
 */
@Mapper
@Repository
public interface ClientMapper extends BaseMapper<Client> {

    /**
     * 根据UserId 查询 客户
     * @param id userid
     * @return 客户
     */
    Client selectClientByUserId(String id);
}
