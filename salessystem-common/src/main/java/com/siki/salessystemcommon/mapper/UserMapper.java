package com.siki.salessystemcommon.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.siki.salessystemcommon.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
/**
 * @author Siki
 * @date 2020/12/29
 */
@Mapper
@Repository
public interface UserMapper extends BaseMapper<User> {
    /**
     * 注册
     * @param map 注册信息
     */
    void saveUserAndRole(Map<String,Object> map);

    /**
     * 查询客户同时查询合同
     * @param id 客户id
     * @return map
     */
    List<Map<String ,Object>> getClientAndContract(String id);
    /**
     * 查询销售人员同时查询合同
     * @param id 客户id
     * @return map
     */
    List<Map<String ,Object>> getSalesAndContract(String id);
}
