package com.siki.salessystemcommon.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.siki.salessystemcommon.entity.Contract;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface ContractMapper extends BaseMapper<Contract> {

//    int insertContract(Contract entity);

    int insertList(Map<String,Object> map);

    /**
     * 根据合同id删除与该合同关联的商品之间的关系
     * @param id 合同id
     */
    int deleteContractAndCommodity(Long id);

    /**
     * 更新合同和商品之间的关系
     * @param map 信息整合
     */
    void updateContractAndCommodity(Map<String,Object> map);

    /**
     * 根据合同id连表查询
     * @param id 合同id
     * @return map
     */
    Map<String ,Object> selectClientSalesAndContract(Long id);
}
