package com.siki.salessystemcommon.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.siki.salessystemcommon.entity.BuyIn;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface BuyInMapper extends BaseMapper<BuyIn> {
}
