package com.siki.salessystemcommon.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.siki.salessystemcommon.entity.Permission;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface PermissionMapper extends BaseMapper<Permission> {

    List<Permission> selectPermissionByRoleId(Long id);
}
