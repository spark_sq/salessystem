package com.siki.salessystemcommon.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.siki.salessystemcommon.entity.Sales;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface SalesMapper extends BaseMapper<Sales> {
    /**
     * 根据UserId 查询Sales
     * @param id UserId
     * @return Slaes
     */
    Sales selectSalesByUserId(String id);
}
