package com.siki.salessystemclient.controller;

import java.security.Principal;

import com.siki.salessystemcommon.dto.CurrentUser;
import com.siki.salessystemcommon.utils.SecurityUtil;
import com.siki.salessystemcommon.utils.SystemMsgJsonResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("/api")
public class TestController {

    @GetMapping("hello")
    @PreAuthorize("hasAnyAuthority('hello')")
    public String hello() {
        return "hello";
    }

    @GetMapping("query")
    @PreAuthorize("hasAnyAuthority('query')")
    public String query() {
        return "具有query权限";
    }


    @GetMapping("test")
    public String test() {
        return "test";
    }

    @GetMapping("current")
    public Principal current(Principal principal) {
        return principal;
    }

    @GetMapping("current1")
    public SystemMsgJsonResponse current1(Authentication authentication) {
        return SystemMsgJsonResponse.success(authentication.getName());
    }

    @GetMapping("current2")
    public CurrentUser current2() {
        return SecurityUtil.currentUser();
    }

}
