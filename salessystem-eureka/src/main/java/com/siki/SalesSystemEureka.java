package com.siki;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class SalesSystemEureka {
    public static void main(String[] args) {
        SpringApplication.run(SalesSystemEureka.class,args);
    }
}
