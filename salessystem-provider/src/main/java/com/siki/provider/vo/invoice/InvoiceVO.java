package com.siki.provider.vo.invoice;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

/**
 * @Author Siki
 * @Date 2020/12/16
 */
@ApiModel("出货单VO")
@Getter
@Setter
@NoArgsConstructor
public class InvoiceVO {
    @ApiModelProperty("出货商品名")
    String commodityName;

    @ApiModelProperty("出货数量")
    String commodityAmount;

    @ApiModelProperty("出货单状态")
    String invoiceStatus;

    public InvoiceVO(Map<String,String> map){
        this.commodityName = map.get("commodityName");
        this.commodityAmount = map.get("commodityAmount");
        this.invoiceStatus = map.get("invoiceStatus");
    }
}
