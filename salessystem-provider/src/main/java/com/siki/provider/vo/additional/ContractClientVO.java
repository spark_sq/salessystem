package com.siki.provider.vo.additional;

import com.siki.provider.vo.contract.ContractCommodityInfoVO;
import com.siki.provider.vo.contract.ContractVO;
import com.siki.provider.vo.user.UserInfoVO;
import com.siki.salessystemcommon.entity.enumeration.ContractStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * @author Siki
 * @date 2020/12/29
 */
@ApiModel("合同关联查询VO")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class ContractClientVO {
    @ApiModelProperty("合同id")
    Long id;

    @ApiModelProperty("合同编号")
    String contractName;

    @ApiModelProperty("合同描述")
    String contractDescription;

    @ApiModelProperty("合同状态")
    String contractStatus;

    @ApiModelProperty("合同总金额")
    Object contractTotalMoney;

    @ApiModelProperty("合同已支付金额")
    Object contractPaidMoney;
}
