package com.siki.provider.vo.contract;


import com.siki.salessystemcommon.entity.Contract;
import com.siki.salessystemcommon.entity.enumeration.ContractStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * @Author Siki
 * @Date 2020/12/11
 */

@ApiModel("合同信息返回VO")
@Getter
@Setter
@NoArgsConstructor
public class ContractVO {
    @ApiModelProperty("合同id")
    Long id;

    @ApiModelProperty("合同创建时间")
    LocalDateTime createTime;

    @ApiModelProperty("合同编号")
    String contractName;

    @ApiModelProperty("合同描述")
    String contractDescription;

    @ApiModelProperty("合同状态")
    String contractStatus;

    @ApiModelProperty("合同总金额")
    String contractTotalMoney;

    @ApiModelProperty("合同已支付金额")
    String contractPaidMoney;

    @ApiModelProperty("商品列表")
    List<ContractCommodityInfoVO> commodityInfoVOList;

    public ContractVO(Contract contracts){
        Optional.ofNullable(contracts).ifPresent(contract -> {
            this.id = contract.getId();
            this.createTime = contract.getCreateTime();
            this.contractName = contract.getContractName();
            this.contractDescription = contract.getContractDescription();
            this.contractStatus = Optional.ofNullable(contract.getContractStatus())
                    .map(ContractStatusEnum::getName).orElse(null);
            this.contractTotalMoney = contract.getContractTotalMoney();
            this.contractPaidMoney = contract.getContractPaidMoney();
        });
    }
}
