package com.siki.provider.vo.purchase;

import com.siki.salessystemcommon.entity.Purchase;
import com.siki.salessystemcommon.entity.enumeration.PurchaseStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Optional;

/**
 * @Author Siki
 * @Date 2020/12/14
 */

@ApiModel("采购清单信息VO")
@Getter
@Setter
@NoArgsConstructor
public class PurchaseVO {
    @ApiModelProperty("清单状态")
    private String purchaseStatus;

    @ApiModelProperty("起始地")
    private String startingPoint;

    @ApiModelProperty("收货地")
    private String endingPoint;

//    @ApiModelProperty("合同id")
//    private Long contractId;

    @ApiModelProperty("采购清单总金额")
    private String purchaseTotalMoney;

    @ApiModelProperty("采购清单已付金额")
    private String purchasePaidMoney;

    @ApiModelProperty("采购商品列表")
    private List<PurchaseCommodityVO> purchaseCommodityVOList;

    public PurchaseVO(Purchase purchases) {
        Optional.ofNullable(purchases).ifPresent(purchase -> {
            this.purchaseStatus = Optional.ofNullable(purchase.getPurchaseStatus())
                    .map(String::valueOf).orElse(null);
            this.startingPoint = purchase.getStartingPoint();
            this.endingPoint = purchase.getEndingPoint();
            // 不确定要不要,暂时先注释
//            this.contractId = purchase.getContractId();
            this.purchaseTotalMoney = purchase.getPurchaseTotalMoney();
            this.purchasePaidMoney = purchase.getPurchasePaidMoney();
        });
    }
}
