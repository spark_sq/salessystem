package com.siki.provider.vo.user;

import com.siki.salessystemcommon.entity.Role;
import com.siki.salessystemcommon.entity.User;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * @author Siki
 * @date 2020/12/27
 */
@ApiModel("用户信息VO")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class UserInfoVO {
    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("用户名")
    private String username;

    @ApiModelProperty("身份")
    private List<String> roles;

    public UserInfoVO(User user) {
        this.id = user.getId();
        this.username = user.getAccount();
    }
}
