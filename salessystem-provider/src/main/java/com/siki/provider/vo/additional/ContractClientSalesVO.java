package com.siki.provider.vo.additional;

import com.siki.provider.vo.contract.ContractCommodityInfoVO;
import com.siki.provider.vo.contract.ContractVO;
import com.siki.provider.vo.user.UserInfoVO;
import com.siki.salessystemcommon.entity.enumeration.ContractStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * @author Siki
 * @date 2020/12/29
 */
@ApiModel("合同关联查询VO")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class ContractClientSalesVO {
    @ApiModelProperty("合同id")
    Long id;

//    @ApiModelProperty("合同创建时间")
//    Object createTime;

    @ApiModelProperty("合同编号")
    String contractName;

    @ApiModelProperty("合同描述")
    String contractDescription;

    @ApiModelProperty("合同状态")
    String contractStatus;

    @ApiModelProperty("合同总金额")
    Object contractTotalMoney;

    @ApiModelProperty("合同已支付金额")
    Object contractPaidMoney;

    @ApiModelProperty("销售人员")
    UserInfoVO sales;

    @ApiModelProperty("客户人员")
    UserInfoVO client;

    public ContractClientSalesVO(Map<String,Object> map){
        this.id = (Long) map.get("id");
        this.contractName = (String) map.get("contract_name");
        this.contractDescription = (String) map.get("contract_description");
        this.contractStatus = ContractStatusEnum.valueOf((String) map.get("contract_status")).getName();
        this.contractTotalMoney = map.get("contract_total_money");
        this.contractPaidMoney = map.get("contract_paid_money");
        UserInfoVO sales = new UserInfoVO();
        sales.setId((String) map.get("sales_id"));
        sales.setUsername((String) map.get("sales_account"));
        this.sales = sales;
        UserInfoVO client = new UserInfoVO();
        client.setId((String) map.get("client_id"));
        client.setUsername((String) map.get("client_account"));
        this.client = client;
    }

//

}
