package com.siki.provider.vo.contract;


import com.siki.salessystemcommon.entity.Commodity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;
import java.util.Optional;

@ApiModel("合同内商品信息VO")
@Getter
@Setter
@NoArgsConstructor
public class ContractCommodityInfoVO {
    @ApiModelProperty("商品id")
    Long id;

    @ApiModelProperty("商品名称")
    String commodityName;

    @ApiModelProperty("商品价格")
    String commodityPrice;

    @ApiModelProperty("商品数量")
    String commodityAmount;

    @ApiModelProperty("未发货数量")
    String commodityNoSendAmount;

    public ContractCommodityInfoVO(Commodity commoditys, Map<String,Integer> amount){
        this.commodityAmount = String.valueOf(amount.get("commodity_amount"));
        this.commodityNoSendAmount = String.valueOf(amount.get("commodity_no_send_amount"));
        Optional.ofNullable(commoditys).ifPresent(commodity -> {
            this.id = commodity.getId();
            this.commodityName = commodity.getCommodityName();
            this.commodityPrice = commodity.getCommodityPrice();
        });
    }
}
