package com.siki.provider.vo.additional;

import com.siki.salessystemcommon.entity.enumeration.ContractStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Siki
 * @date 2020/12/29
 */
@ApiModel("客户关联查询VO")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class UserContractVO {
    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("用户名")
    private String username;

    @ApiModelProperty("合同vo")
    private List<ContractClientVO> contract;

    public ContractClientVO getContractVO(Map<String ,Object > map) {
        ContractClientVO contractClientVO = new ContractClientVO();
        contractClientVO.setContractName((String) map.get("contract_name"));
        contractClientVO.setContractStatus(ContractStatusEnum
                .valueOf((String) map.get("contract_status")).getName());
        contractClientVO.setContractDescription((String) map.get("contract_description"));
        contractClientVO.setContractTotalMoney(map.get("contract_total_money"));
        contractClientVO.setContractPaidMoney(map.get("contract_paid_money"));
        contractClientVO.setId((Long) map.get("c_id"));
        return contractClientVO;
    }

    public UserContractVO(List<Map<String ,Object >> map) {
        this.id = (String) map.get(0).get("u_id");
        this.username = (String) map.get(0).get("account");

        this.contract = map.stream().map(this::getContractVO)
                .collect(Collectors.toList());
    }
}
