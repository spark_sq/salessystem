package com.siki.provider.vo.commodity;


import com.siki.salessystemcommon.entity.Commodity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Optional;

@ApiModel("商品信息返回VO")
@Getter
@Setter
@NoArgsConstructor
public class CommodityVO {
    @ApiModelProperty("商品id")
    Long id;

    @ApiModelProperty("商品名称")
    String commodityName;

    @ApiModelProperty("商品价格")
    String commodityPrice;

    @ApiModelProperty("商品库存")
    Integer commodityInventory;

    @ApiModelProperty("商品图片")
    String commodityImage;

    public CommodityVO(Commodity commoditys){
        Optional.ofNullable(commoditys).ifPresent(commodity -> {
            this.id = commodity.getId();
            this.commodityName = commodity.getCommodityName();
            this.commodityPrice = commodity.getCommodityPrice();
            this.commodityInventory = commodity.getCommodityInventory();
            this.commodityImage = commodity.getCommodityImage();
        });
    }
}
