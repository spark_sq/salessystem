package com.siki.provider.vo.purchase;


import com.siki.salessystemcommon.entity.Commodity;
import com.siki.salessystemcommon.utils.DecimalCalculation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;
import java.util.Optional;

/**
 * @Author Siki
 * @Date 2020/12/14
 */

@ApiModel("采购清单对应商品数据VO")
@Getter
@Setter
@NoArgsConstructor
public class PurchaseCommodityVO {
    @ApiModelProperty("商品id")
    Long id;

    @ApiModelProperty("商品名称")
    String commodityName;

    @ApiModelProperty("采购数量")
    String commodityAmount;

    @ApiModelProperty("商品总价")
    String commodityTotalAmount;

    @ApiModelProperty("商品状态")
    String shipped;

    public PurchaseCommodityVO(Commodity commoditys, Map<String, Object> amount) {
        this.commodityAmount = String.valueOf(amount.get("commodityAmount"));
        this.shipped = amount.get("shipped").equals(1) ? "已出货" : "未出货";
        Optional.ofNullable(commoditys).ifPresent(commodity -> {
            this.id = commodity.getId();
            this.commodityName = commodity.getCommodityName();
            this.commodityTotalAmount = DecimalCalculation
                    .mul(commodity.getCommodityPrice(), this.commodityAmount);
        });

    }
}
