package com.siki.provider.vo.buyin;


import com.siki.salessystemcommon.entity.BuyIn;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Optional;

/**
 * @author Siki
 * @date 2020/12/29
 */
@ApiModel("进货单VO")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class BuyInVO {
    @ApiModelProperty("商品名称")
    private String commodityName;

    @ApiModelProperty("商品数量")
    private Integer commodityAmount;

    @ApiModelProperty("仓库管理员名称")
    private String userName;

    public BuyInVO(BuyIn buyIns,String name){
        this.userName = name;
        Optional.ofNullable(buyIns).ifPresent(buyIn -> {
            this.commodityName = buyIn.getCommodityName();
            this.commodityAmount = buyIn.getCommodityAmount();
        });
    }
}
