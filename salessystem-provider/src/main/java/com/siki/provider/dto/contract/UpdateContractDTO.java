package com.siki.provider.dto.contract;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.List;
/**
 * @author Siki
 * @date 2020/12/29
 */
@ApiModel("更新合同DTO")
@Getter
@Setter
@NoArgsConstructor
public class UpdateContractDTO {
    @ApiModelProperty("合同id")
    Long id;

    @ApiModelProperty("合同描述")
    String contractDescription;

    @ApiModelProperty("合同状态")
    @NotNull(message = "合同状态不能为空")
    String contractStatus;

    @ApiModelProperty("商品")
    List<BuyCommodityListDTO> signingContractCommodityDTOList;
}
