package com.siki.provider.dto.purchase;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@ApiModel("分页查询采购清单DTO")
@Getter
@Setter
@NoArgsConstructor
public class PagePurchaseDTO {
    @ApiModelProperty("合同id")
    @NotNull(message = "合同id不能为空")
    Long contractId;

    @ApiModelProperty("清单状态")
    String purchaseStatus;

    @ApiModelProperty("当前页码")
    Long pageNo;

    @ApiModelProperty("返回数据条数")
    Long pageSize;
}
