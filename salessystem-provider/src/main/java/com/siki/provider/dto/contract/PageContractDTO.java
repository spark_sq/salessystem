package com.siki.provider.dto.contract;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Siki
 * @date 2020/12/29
 */
@ApiModel("分页查询合同DTO")
@Getter
@Setter
@NoArgsConstructor
@ToString

public class PageContractDTO {
    @ApiModelProperty("合同名")
    String contractName;

    @ApiModelProperty("合同状态")
    String contractStatus;

    @ApiModelProperty("当前页码")
    Long pageNo;

    @ApiModelProperty("返回数据条数")
    Long pageSize;
}
