package com.siki.provider.dto.buyin;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * @Author Siki
 * @Date 2020/12/18
 */
@ApiModel("生成进货单DTO")
@Getter
@Setter
@NoArgsConstructor
public class SaveBuyInDTO {
    @ApiModelProperty("商品id")
    @NotNull(message = "商品id不能为空")
    private Long commodityId;

    @ApiModelProperty("商品数量")
    @NotNull(message = "商品数量不能为空")
    private Integer commodityAmount;
}
