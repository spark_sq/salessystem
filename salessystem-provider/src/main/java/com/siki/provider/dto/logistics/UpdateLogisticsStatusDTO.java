package com.siki.provider.dto.logistics;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * @Author Siki
 * @Date 2020/12/17
 */
@ApiModel("物流更改状态DTO")
@Getter
@Setter
@NoArgsConstructor
public class UpdateLogisticsStatusDTO {
    @ApiModelProperty("物流状态")
    @NotNull(message = "物流状态不能为空")
    private String status;

    @ApiModelProperty("收货地")
    @NotNull(message = "收货地不能为空")
    private String endingPoint;

    @ApiModelProperty("物流id")
    @NotNull(message = "物流id不能为空")
    private Long logisticsId;
}
