package com.siki.provider.dto.contract;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@ApiModel("商品列表DTO")
@Getter
@Setter
@NoArgsConstructor
public class BuyCommodityListDTO {
    @ApiModelProperty("商品id")
    @NotNull(message = "商品id不能为空")
    Long id;

    /**
     * 备注：根据使用场景不同,字段代表的意义不同
     * 应用在合同时,字段表示: 该合同订购商品的数量
     * 应用在采购清单时,字段表示: 采购清单从合同中提取的商品数量
     */
    @ApiModelProperty("商品数量")
    @NotNull(message = "商品数量不能为空")
    String commodityAmount;

    @ApiModelProperty("商品未发货数量")
    String commodityNoSendAmount;

    @ApiModelProperty("商品发货状态")
    Integer shipped;
}
