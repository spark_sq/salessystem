package com.siki.provider.dto.purchase;

import com.siki.provider.dto.contract.BuyCommodityListDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Author Siki
 * @Date 2020/12/14
 */

@ApiModel("更新采购清单DTO")
@Getter
@Setter
@NoArgsConstructor
public class UpdatePurchaseDTO {
    @ApiModelProperty("采购清单id")
    @NotNull(message = "采购清单id不能为空")
    private Long id;

    @ApiModelProperty("起始地")
    @NotNull(message = "起始地不能为空")
    private String startingPoint;

    @ApiModelProperty("收货地")
    @NotNull(message = "收货地不能为空")
    private String endingPoint;

    @ApiModelProperty("采购商品列表")
    @NotNull(message = "商品列表不能为空")
    private List<BuyCommodityListDTO> purchaseCommodityList;
}
