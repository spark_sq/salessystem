package com.siki.provider.dto.contract;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Author Siki
 * @Date 2020/12/11
 */

@ApiModel("签订合同DTO")
@Getter
@Setter
@NoArgsConstructor
public class SigningContractDTO {
    @ApiModelProperty("合同id")
    Long id;

    @ApiModelProperty("客户id")
    @NotNull(message = "客户id不能为空")
    String clientId;

    @ApiModelProperty("合同编号")
    @NotNull(message = "合同编号不能为空")
    String contractName;

    @ApiModelProperty("合同描述")
    @NotNull(message = "合同描述不能为空")
    String contractDescription;

    @ApiModelProperty("商品")
    @NotNull(message = "商品信息不能为空")
    List<BuyCommodityListDTO> signingContractCommodityDTOList;
}
