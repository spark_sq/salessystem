package com.siki.provider.dto.buyin;

import io.swagger.annotations.ApiModelProperty;


import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Siki
 * @date 2020/12/29
 */

@ApiModel("分页查询进货单接口")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class PageBuyInDTO {
    @ApiModelProperty("进货人员名称")
    String userName;

    @ApiModelProperty("商品名称")
    String commodityName;

    @ApiModelProperty("当前页码")
    Long pageNo;

    @ApiModelProperty("返回数据条数")
    Long pageSize;
}
