package com.siki.provider.dto.invoice;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
/**
 * @Author Siki
 * @Date 2020/12/15
 */
@ApiModel("生成出货单DTO")
@Getter
@Setter
@NoArgsConstructor
public class SaveInvoiceDTO {
    @ApiModelProperty("购物清单id")
    @NotNull(message = "购物清单id不能为空")
    Long purchaseId;

    @ApiModelProperty("商品id")
    @NotNull(message = "商品id不能为空")
    Long commodityId;

    @ApiModelProperty("商品数量")
    @NotNull(message = "商品数量不能为空")
    Integer commodityAmount;
}
