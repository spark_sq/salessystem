package com.siki.provider.dto.logistics;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @Author Siki
 * @Date 2020/12/17
 */

@ApiModel("生成物流DTO")
@Getter
@Setter
@NoArgsConstructor
public class SaveLogisticsDTO {
    @ApiModelProperty("出货单id")
    private Long invoiceId;
}
