package com.siki.provider.dto.commodity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
/**
 * @author Siki
 * @date 2020/12/29
 */
@ApiModel("分页查询采购清单DTO")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class PageCommodityDTO {
    @ApiModelProperty("库存区间开始")
    Integer amountStart;

    @ApiModelProperty("库存区间结束")
    Integer amountEnd;

    @ApiModelProperty("价格区间开始")
    Integer priceStart;

    @ApiModelProperty("价格区间结束")
    Integer priceEnd;

    @ApiModelProperty("当前页码")
    Long pageNo;

    @ApiModelProperty("返回数据条数")
    Long pageSize;
}
