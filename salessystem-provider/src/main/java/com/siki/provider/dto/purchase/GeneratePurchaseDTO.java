package com.siki.provider.dto.purchase;


import com.siki.provider.dto.contract.BuyCommodityListDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.util.List;


@ApiModel("生成采购清单DTO")
@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
public class GeneratePurchaseDTO {
    @ApiModelProperty("采购清单id")
    private Long id;

    @ApiModelProperty("起始地")
    @NotNull(message = "起始地不能为空")
    private String startingPoint;

    @ApiModelProperty("收货地")
    @NotNull(message = "收货地不能为空")
    private String endingPoint;

    @ApiModelProperty("合同id")
    @NotNull(message = "合同id不能为空")
    private Long contractId;

    @ApiModelProperty("清单已付款金额")
    private String commodityPaidMoney;

    @ApiModelProperty("采购商品列表")
    @NotNull(message = "商品列表不能为空")
    private List<BuyCommodityListDTO> purchaseCommodityList;
}
