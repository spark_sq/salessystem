package com.siki.provider.dto.commodity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@ApiModel("增加商品DTO")
@Getter
@Setter
@NoArgsConstructor
public class AddCommodityDTO {
    @ApiModelProperty("商品id")
    Long id;

    @NotNull(message = "商品名称不能为空")
    @ApiModelProperty("商品名称")
    String commodityName;

    @NotNull(message = "商品价格不能为空")
    @ApiModelProperty("商品价格")
    String commodityPrice;

    @NotNull(message = "商品库存不能为空")
    @ApiModelProperty("商品库存")
    Integer commodityInventory;

    @ApiModelProperty("商品图片")
    String commodityImage;
}
