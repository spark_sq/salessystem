package com.siki.provider.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siki.provider.dto.buyin.PageBuyInDTO;
import com.siki.provider.dto.buyin.SaveBuyInDTO;
import com.siki.provider.service.BuyInService;
import com.siki.provider.vo.buyin.BuyInVO;
import com.siki.provider.vo.purchase.PurchaseVO;
import com.siki.salessystemcommon.entity.BuyIn;
import com.siki.salessystemcommon.entity.Commodity;
import com.siki.salessystemcommon.entity.Purchase;
import com.siki.salessystemcommon.mapper.BuyInMapper;
import com.siki.salessystemcommon.mapper.CommodityMapper;
import com.siki.salessystemcommon.mapper.UserMapper;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @Author Siki
 * @Date 2020/12/18
 */
@Service
public class BuyInServiceImpl implements BuyInService {

    private final BuyInMapper buyInMapper;
    private final CommodityMapper commodityMapper;
    private final UserMapper userMapper;

    public BuyInServiceImpl(BuyInMapper buyInMapper, CommodityMapper commodityMapper, UserMapper userMapper) {
        this.buyInMapper = buyInMapper;
        this.commodityMapper = commodityMapper;
        this.userMapper = userMapper;
    }

    @Override
    public void saveBuyIn(SaveBuyInDTO dto, String id) {
        BuyIn buyIn = new BuyIn();
        var commodity = getCommodityById(dto.getCommodityId());
        buyIn.setCommodityAmount(dto.getCommodityAmount())
                .setCommodityId(dto.getCommodityId())
                .setUserId(id)
                .setCommodityName(commodity.getCommodityName());
        buyInMapper.insert(buyIn);
        // 增加商品库存
        // 现库存数量 = 旧库存数量 + 进货单进货的数量
        commodity.setCommodityInventory(commodity.getCommodityInventory() +
                dto.getCommodityAmount());
        commodityMapper.updateById(commodity);
    }

    @Override
    public BuyInVO getBuyIn(Long id) {
        BuyIn buyIn = getBuyInById(id);
        return new BuyInVO(buyIn,userMapper.selectById(buyIn.getUserId()).getAccount());
    }

    @Override
    public IPage<BuyInVO> page(PageBuyInDTO dto) {
        QueryWrapper<BuyIn> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("commodity_name",dto.getCommodityName());
        return new Page<BuyInVO>(dto.getPageNo(), dto.getPageSize())
                .setRecords(buyInMapper.selectPage(new Page<>(dto.getPageNo(),
                                dto.getPageSize()),
                        queryWrapper)
                        .getRecords()
                        .stream().map(BuyIn::getId)
                        .map(this::getBuyIn)
                        .collect(Collectors.toList()));
    }

    /**
     * [私有方法] - 根据id查询进货单
     * @param id 商品id
     * @return BuyIn 商品
     */
    private BuyIn getBuyInById(Long id){
        return Optional.ofNullable(buyInMapper.selectById(id))
                .orElseThrow(() -> new RuntimeException("该id:" + id + "有误"));
    }
    /**
     * [私有方法] - 根据id查询商品
     * @param id 商品id
     * @return Commodity 商品
     */
    private Commodity getCommodityById(Long id) {
        return Optional.ofNullable(commodityMapper.selectById(id))
                .orElseThrow(() -> new RuntimeException("该id:" + id + "有误"));
    }
}
