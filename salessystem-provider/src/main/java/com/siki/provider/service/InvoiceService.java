package com.siki.provider.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.siki.provider.dto.invoice.SaveInvoiceDTO;
import com.siki.provider.vo.invoice.InvoiceVO;

/**
 * @Author Siki
 * @Date 2020/12/15
 */
public interface InvoiceService {

    /**
     * 生成出货单
     *
     * @param dto 出货单信息
     */
    void saveInvoice(SaveInvoiceDTO dto);

    /**
     * 根据id删除出货单
     *
     * @param id 出货单id
     */
    void deleteInvoice(Long id);

    /**
     * 支付出货单
     *
     * @param id 出货单id
     */
    void paidInvoice(Long id);

    /**
     * 根据id获取出货单详情
     *
     * @param id 出货单id
     * @return InvoiceVO
     */
    InvoiceVO getInvoice(Long id);

    /**
     * 分页查询出货单信息
     * @param purchaseId 清单id
     * @param pageNo 分页页数
     * @param pageSize 分页大小
     * @return 出货单列表
     */
    IPage<InvoiceVO> pageInvoice(Long purchaseId, Long pageNo, Long pageSize);
}
