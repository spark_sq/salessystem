package com.siki.provider.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.siki.provider.dto.commodity.AddCommodityDTO;
import com.siki.provider.dto.commodity.PageCommodityDTO;
import com.siki.provider.vo.commodity.CommodityVO;

/**
 * @Author Siki
 * @Date 2020/11/26
 */
public interface CommodityService {
    /**
     * 增加一个商品
     *
     * @param dto 增加商品dto
     */
    void addCommodity(AddCommodityDTO dto);
    /**
     * 删除一个商品
     *
     * @param id 商品id
     */
    void deleteCommodity(Long id);

    /**
     * 根据ID获取商品
     *
     * @param id 商品id
     * @return
     */
    CommodityVO getCommodity(Long id);

    /**
     * 条件查询商品
     * @param dto 条件
     * @return 商品
     */
    IPage<CommodityVO> page(PageCommodityDTO dto);
}
