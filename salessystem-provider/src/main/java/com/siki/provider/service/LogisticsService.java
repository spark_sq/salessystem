package com.siki.provider.service;

import com.siki.provider.dto.logistics.SaveLogisticsDTO;
import com.siki.provider.dto.logistics.UpdateLogisticsStatusDTO;

public interface LogisticsService {
    /**
     * 生成物流接口
     * @param dto 物流信息
     */
    void saveLogisticsInfo(SaveLogisticsDTO dto);

    /**
     * 修改物流状态
     * @param dto 物流修改dto
     */
    void updateLogisticsStatus(UpdateLogisticsStatusDTO dto);

    /**
     * 确认收货
     * @param id 物流id
     */
    void reLogistics(Long id);
}
