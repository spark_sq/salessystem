package com.siki.provider.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.siki.provider.dto.commodity.PageCommodityDTO;
import com.siki.provider.vo.buyin.BuyInVO;
import com.siki.salessystemcommon.entity.BuyIn;
import com.siki.salessystemcommon.entity.Commodity;
import com.siki.salessystemcommon.mapper.CommodityMapper;
import com.siki.provider.dto.commodity.AddCommodityDTO;
import com.siki.provider.service.CommodityService;
import com.siki.provider.vo.commodity.CommodityVO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @Author Siki
 * @Date 2020/12/16
 */
@Service
@RequiredArgsConstructor
public class CommodityServiceImpl implements CommodityService {


    private final CommodityMapper commodityMapper;

    @Override
    public void addCommodity(AddCommodityDTO dto) {
        Optional.ofNullable(dto.getId()).ifPresentOrElse(dt -> {
            updateCommodity(dto);
        }, () -> {
            Commodity commodity = new Commodity();
            commodity.setCommodityName(dto.getCommodityName())
                    .setCommodityPrice(dto.getCommodityPrice())
                    .setCommodityInventory(dto.getCommodityInventory())
                    .setCommodityImage(Optional
                            .ofNullable(dto.getCommodityImage())
                            .orElse(null));
            commodityMapper.insert(commodity);
        });

    }

    @Override
    public void deleteCommodity(Long id) {
        commodityMapper.deleteById(id);
    }

    @Override
    public CommodityVO getCommodity(Long id) {
        return new CommodityVO(getCommodityById(id));
    }

    @Override
    public IPage<CommodityVO> page(PageCommodityDTO dto) {
        QueryWrapper<Commodity> queryWrapper = new QueryWrapper<>();
        queryWrapper.between("commodity_inventory",
                Optional.ofNullable(dto.getAmountStart()).orElse(0),
                Optional.ofNullable(dto.getAmountEnd()).orElse(Integer.MAX_VALUE)
        ).between(
                "commodity_price",
                Optional.ofNullable(dto.getPriceStart()).orElse(0),
                Optional.ofNullable(dto.getPriceEnd()).orElse(Integer.MAX_VALUE)
        );
        return new Page<CommodityVO>(dto.getPageNo(), dto.getPageSize())
                .setRecords(commodityMapper.selectPage(new Page<>(dto.getPageNo(),
                                dto.getPageSize()),
                        queryWrapper)
                        .getRecords()
                        .stream().map(Commodity::getId)
                        .map(this::getCommodity)
                        .collect(Collectors.toList()));
    }

    /**
     *
     */

    /**
     * [私有方法] - 更新商品
     *
     * @param dto 商品信息dto
     */
    private void updateCommodity(AddCommodityDTO dto) {
        Commodity commodity = getCommodityById(dto.getId());
        commodity.setCommodityName(dto.getCommodityName())
                .setCommodityInventory(dto.getCommodityInventory())
                .setCommodityPrice(dto.getCommodityPrice())
                .setCommodityImage(dto.getCommodityImage());
        commodityMapper.updateById(commodity);
    }

    /**
     * [私有方法] - 根据id获取商品
     *
     * @param id 商品id
     * @return Commodity 商品
     */
    private Commodity getCommodityById(Long id) {
        return Optional.ofNullable(commodityMapper.selectById(id))
                .orElseThrow(() -> new RuntimeException("该id:" + id + "有误"));
    }
}
