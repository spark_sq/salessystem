package com.siki.provider.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.siki.provider.dto.purchase.GeneratePurchaseDTO;
import com.siki.provider.dto.purchase.PagePurchaseDTO;
import com.siki.provider.dto.purchase.UpdatePurchaseDTO;
import com.siki.provider.vo.purchase.PurchaseVO;
/**
 * @author Siki
 * @date 2020/12/28
 */
public interface PurchaseService {
    /**
     * 生成采购清单
     *
     * @param dto 生成采购清单信息dto
     */
    void generatePurchase(GeneratePurchaseDTO dto);

    /**
     * 删除采购清单
     * @param id 采购清单id
     */
    void deletePurchase(Long id);

    /**
     * 更新采购清单
     * @param dto 采购清单更新信息
     */
    void updatePurchase(UpdatePurchaseDTO dto);

    /**
     * 根据id查询采购清单
     * @param id 采购清单ID
     * @return 采购清单详细信息
     */
    PurchaseVO getPurchase(Long id);

    /**
     * 分页查询所有采购清单
     * @param dto 分页查询条件
     * @return 采购清单集合
     */
    IPage<PurchaseVO> getAllPurchase(PagePurchaseDTO dto);
}
