package com.siki.provider.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.siki.provider.dto.contract.PageContractDTO;
import com.siki.provider.dto.contract.SigningContractDTO;
import com.siki.provider.dto.contract.UpdateContractDTO;
import com.siki.provider.vo.contract.ContractVO;
import com.siki.salessystemcommon.entity.Contract;

import java.util.List;
/**
 * @author Siki
 * @date 2020/12/28
 */
public interface ContractService {
    /**
     * 签署合同
     * @param dto 签署合同数据dto
     * @param id 用户id
     */
    void signingContract(SigningContractDTO dto, String id);

    /**
     * 根据id查询合同详情
     * @param id 合同id
     * @return ContractVO
     */
    ContractVO getContract(Long id);

    /**
     * 修改合同信息
     * @param dto 合同信息dto
     * @param id 修改人id
     */
    void updateContract(UpdateContractDTO dto, String id);

    /**
     * 删除合同
     * @param id 合同id
     */
    void deleteContract(Long id);

    /**
     * 分页查询所有的合同
     * @param dto 分页查询dto
     * @param id 销售人员id
     * @return
     */
    IPage<ContractVO> getPageAllContract(PageContractDTO dto, String id);
}
