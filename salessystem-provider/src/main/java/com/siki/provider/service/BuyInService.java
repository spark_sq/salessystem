package com.siki.provider.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.siki.provider.dto.buyin.PageBuyInDTO;
import com.siki.provider.dto.buyin.SaveBuyInDTO;
import com.siki.provider.vo.buyin.BuyInVO;

/**
 * @Author Siki
 * @Date 2020/12/18
 */
public interface BuyInService {

    /**
     * 生成进货单
     *
     * @param dto 进货单信息
     * @param id 当前用户id
     */
    void saveBuyIn(SaveBuyInDTO dto, String id);

    /**
     * 根据id查询进货单信息
     * @param id 进货单id
     * @return 进货单
     */
    BuyInVO getBuyIn(Long id);

    IPage<BuyInVO> page(PageBuyInDTO dto);
}
