package com.siki.provider.controller;

import cn.hutool.core.lang.Assert;
import com.siki.provider.vo.additional.ContractClientSalesVO;
import com.siki.provider.vo.additional.ContractClientVO;
import com.siki.provider.vo.additional.UserContractVO;
import com.siki.provider.vo.commodity.CommodityVO;
import com.siki.provider.vo.contract.ContractCommodityInfoVO;
import com.siki.salessystemcommon.mapper.CommodityMapper;
import com.siki.salessystemcommon.mapper.ContractMapper;
import com.siki.salessystemcommon.mapper.UserMapper;
import com.siki.salessystemcommon.utils.SystemMsgJsonResponse;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Siki
 * @date 2020/12/29
 */
@Api(tags = "选做接口")
@RequestMapping("/api/vi/siki/additional")
@RestController
public class AdditionalController {
    private final ContractMapper contractMapper;
    private final UserMapper userMapper;

    public AdditionalController(ContractMapper contractMapper, UserMapper userMapper) {
        this.contractMapper = contractMapper;
        this.userMapper = userMapper;
    }

    @PostMapping("/getClientSalesAndContract/{id}")
    public SystemMsgJsonResponse get1(@PathVariable("id") Long id){
        Map<String, Object> stringObjectMap = contractMapper.selectClientSalesAndContract(id);
        return SystemMsgJsonResponse.success(new ContractClientSalesVO(stringObjectMap));
    }

    @PostMapping("/getClientAndContract/{id}")
    public SystemMsgJsonResponse get2(@PathVariable("id") String id){
        List<Map<String, Object>> stringObjectMap = userMapper.getClientAndContract(id);
        return SystemMsgJsonResponse.success(new UserContractVO(stringObjectMap));
    }

    @PostMapping("/getSalesAndContract/{id}")
    public SystemMsgJsonResponse get3(@PathVariable("id") String id){
        List<Map<String, Object>> stringObjectMap = userMapper.getSalesAndContract(id);
        return SystemMsgJsonResponse.success(new UserContractVO(stringObjectMap));
    }
}
