package com.siki.provider.controller;

import com.siki.provider.dto.buyin.PageBuyInDTO;
import com.siki.provider.dto.buyin.SaveBuyInDTO;
import com.siki.provider.service.BuyInService;
import com.siki.salessystemcommon.utils.SystemMsgJsonResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author Siki
 * @date 2020/12/27
 */
@Api(tags = "进货单相关接口模块")
@RestController
@RequestMapping("/api/vi/siki/buyIn")
public class BuyInController {

    private final BuyInService buyInService;

    public BuyInController(BuyInService buyInService) {
        this.buyInService = buyInService;
    }

    @PostMapping("/saveBuyIn")
    @ApiOperation(value = "生成进货单接口",httpMethod = "POST")
    public SystemMsgJsonResponse saveBuyIn(@RequestBody @Validated SaveBuyInDTO dto,
                                           Authentication authentication){
        buyInService.saveBuyIn(dto,authentication.getName());
        return SystemMsgJsonResponse.success();
    }

    @GetMapping("/getBuyIn/{id}")
    @ApiOperation(value = "查询进货单接口",httpMethod = "GET")
    public SystemMsgJsonResponse getBuyIn(@PathVariable("id") Long id){
        return SystemMsgJsonResponse.success(buyInService.getBuyIn(id));
    }

    @PostMapping("/page")
    @ApiOperation(value = "分页查询所有进货单",httpMethod = "POST")
    public SystemMsgJsonResponse page(@RequestBody @Validated PageBuyInDTO dto){
        return SystemMsgJsonResponse.success(buyInService.page(dto));
    }
}
