package com.siki.provider.controller;

import com.siki.provider.dto.purchase.GeneratePurchaseDTO;
import com.siki.provider.dto.purchase.PagePurchaseDTO;
import com.siki.provider.dto.purchase.UpdatePurchaseDTO;
import com.siki.provider.service.PurchaseService;
import com.siki.salessystemcommon.utils.SystemMsgJsonResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * @Author Siki
 * @Date 2020/12/14
 */

@Api(tags = "采购清单相关接口")
@RequestMapping("/api/vi/siki/purchase")
@RestController
public class PurchaseController {
    private final PurchaseService purchaseService;

    public PurchaseController(PurchaseService purchaseService) {
        this.purchaseService = purchaseService;
    }

    @PostMapping("/generatePurchase")
    @ApiOperation(value = "生成采购清单接口",httpMethod = "POST",
            consumes = "application/json")
    public SystemMsgJsonResponse GeneratePurchase(@RequestBody @Validated GeneratePurchaseDTO dto){
        purchaseService.generatePurchase(dto);
        return SystemMsgJsonResponse.success();
    }

    @PostMapping("/deletePurchase/{id}")
    @ApiOperation(value = "删除采购清单接口",httpMethod = "POST",
            consumes = "application/json")
    public SystemMsgJsonResponse deletePurchase(@PathVariable("id") @NotNull(message = "采购清单id不能为空") Long id){
        purchaseService.deletePurchase(id);
        return SystemMsgJsonResponse.success();
    }

    @PostMapping("/updatePurchase")
    @ApiOperation(value = "更新采购清单接口",httpMethod = "POST",
            consumes = "application/json")
    public SystemMsgJsonResponse updatePurchase(@RequestBody @Validated UpdatePurchaseDTO dto){
        purchaseService.updatePurchase(dto);
        return SystemMsgJsonResponse.success();
    }

    @GetMapping("/getPurchase/{id}")
    @ApiOperation(value = "根据id查询采购清单",httpMethod = "GET")
    public SystemMsgJsonResponse getPurchaseById(@PathVariable("id") @NotNull(message = "采购清单id不能为空") Long id){
        return SystemMsgJsonResponse.success(purchaseService.getPurchase(id));
    }

    @PostMapping("/getAllPurchase")
    @ApiOperation(value = "根据合同id查询全部采购清单",httpMethod = "POST")
    public SystemMsgJsonResponse getAllPurchase(@RequestBody @Validated PagePurchaseDTO dto){
        return SystemMsgJsonResponse.success(purchaseService.getAllPurchase(dto));
    }
}
