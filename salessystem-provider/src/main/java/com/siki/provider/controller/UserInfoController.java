package com.siki.provider.controller;

import com.siki.provider.vo.user.UserInfoVO;
import com.siki.salessystemcommon.entity.Role;
import com.siki.salessystemcommon.entity.User;
import com.siki.salessystemcommon.mapper.RoleMapper;
import com.siki.salessystemcommon.mapper.UserMapper;
import com.siki.salessystemcommon.utils.SystemMsgJsonResponse;
import io.swagger.annotations.Api;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Siki
 * @date 2020/12/27
 */

@Api(tags = "角色相关接口模块")
@RestController
@RequestMapping("/api/vi/siki/user")
public class UserInfoController {

    private final RoleMapper roleMapper;
    private final UserMapper userMapper;

    public UserInfoController(RoleMapper roleMapper, UserMapper userMapper) {
        this.roleMapper = roleMapper;
        this.userMapper = userMapper;
    }


    @GetMapping("/getUserInfo")
    public SystemMsgJsonResponse getUserInfo(Authentication auth){
        UserInfoVO userInfoVO = new UserInfoVO(getUserById(auth.getName()));
        List<String> collect = roleMapper.selectRoleByUserId(auth.getName()).stream()
                .map(Role::getRoleName).collect(Collectors.toList());
        userInfoVO.setRoles(collect);
        return SystemMsgJsonResponse.success(userInfoVO);
    }

    private User getUserById(String id) {
        return Optional.ofNullable(userMapper.selectById(id))
                .orElseThrow(() -> new RuntimeException("该id:" + id + "有误"));
    }
}
