package com.siki.provider.controller;

import com.siki.provider.dto.logistics.SaveLogisticsDTO;
import com.siki.provider.dto.logistics.UpdateLogisticsStatusDTO;
import com.siki.provider.service.LogisticsService;
import com.siki.salessystemcommon.utils.SystemMsgJsonResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author Siki
 * @date 2020/12/29
 */
@Api(tags = "物流信息相关接口")
@RequestMapping("/api/vi/siki/logistics")
@RestController
public class LogisticsController {

    private final LogisticsService logisticsService;

    public LogisticsController(LogisticsService logisticsService) {
        this.logisticsService = logisticsService;
    }

    @PostMapping("/saveLogistics")
    @ApiOperation(value = "生成物流接口",httpMethod = "POST")
    public SystemMsgJsonResponse saveLogistics(@RequestBody @Validated SaveLogisticsDTO dto){
        logisticsService.saveLogisticsInfo(dto);
        return SystemMsgJsonResponse.success();
    }

    @PostMapping("/updateLogistics")
    @ApiOperation(value = "更改物流信息接口",httpMethod = "POST")
    public SystemMsgJsonResponse updateLogistics(@RequestBody @Validated UpdateLogisticsStatusDTO dto){
        logisticsService.updateLogisticsStatus(dto);
        return SystemMsgJsonResponse.success();
    }

    @PostMapping("/reLogistics/{id}")
    @ApiOperation(value = "确认收货接口",httpMethod = "POST")
    public SystemMsgJsonResponse reLogistics(@PathVariable("id") Long id){
        logisticsService.reLogistics(id);
        return SystemMsgJsonResponse.success();
    }
}
