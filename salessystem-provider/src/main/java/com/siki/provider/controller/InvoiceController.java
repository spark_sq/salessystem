package com.siki.provider.controller;

import com.siki.provider.dto.invoice.SaveInvoiceDTO;
import com.siki.provider.service.InvoiceService;
import com.siki.salessystemcommon.utils.SystemMsgJsonResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
/**
 * @Author Siki
 * @Date 2020/12/16
 */
@RestController
@Api(tags = "发货单相关接口")
@RequestMapping("/api/vi/siki/invoice")
public class InvoiceController {
    private final InvoiceService invoiceService;

    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    @PostMapping("/saveInvoice")
    @ApiOperation(value = "付款生成出货单接口", httpMethod = "POST")
    public SystemMsgJsonResponse saveInvoice(@RequestBody @Validated SaveInvoiceDTO dto) {
        invoiceService.saveInvoice(dto);
        return SystemMsgJsonResponse.success();
    }

    @PostMapping("/deleteInvoice/{id}")
    @ApiOperation(value = "根据id删除出货单接口", httpMethod = "POST")
    public SystemMsgJsonResponse deleteInvoice(@PathVariable("id") Long id) {
        invoiceService.deleteInvoice(id);
        return SystemMsgJsonResponse.success();
    }

    @PostMapping("/paidInvoice/{id}")
    @ApiOperation(value = "支付出货单接口", httpMethod = "POST")
    public SystemMsgJsonResponse paidInvoice(@PathVariable("id") Long id) {
        invoiceService.paidInvoice(id);
        return SystemMsgJsonResponse.success();
    }

    @GetMapping("/getInvoice/{id}")
    @ApiOperation(value = "根据id获得出货单的详细信息", httpMethod = "GET")
    public SystemMsgJsonResponse getInvoice(@PathVariable("id") Long id) {
        return SystemMsgJsonResponse.success(invoiceService.getInvoice(id));
    }

    @GetMapping(value = "/pageInvoice")
    @ApiOperation(value = "分页查询出货单", httpMethod = "GET")
    public SystemMsgJsonResponse pageInvoice(@RequestParam("purchaseId") Long purchaseId,
                                             @RequestParam("pageNo") Long pageNo,
                                             @RequestParam("pageSize") Long pageSize){
        return SystemMsgJsonResponse.success(invoiceService.pageInvoice(purchaseId,pageNo,pageSize));
    }

}
