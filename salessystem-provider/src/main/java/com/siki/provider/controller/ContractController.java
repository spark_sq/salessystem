package com.siki.provider.controller;

import com.siki.provider.dto.contract.PageContractDTO;
import com.siki.provider.dto.contract.SigningContractDTO;
import com.siki.provider.dto.contract.UpdateContractDTO;
import com.siki.provider.service.ContractService;
import com.siki.provider.vo.contract.ContractCommodityInfoVO;
import com.siki.provider.vo.contract.ContractVO;
import com.siki.salessystemcommon.utils.SystemMsgJsonResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
/**
 * @author Siki
 * @date 2020/12/28
 */
@Api(tags = "合同相关接口")
@RequestMapping("/api/vi/siki/contract")
@RestController
public class ContractController {
    ContractService contractService;

    public ContractController(ContractService contractService) {
        this.contractService = contractService;
    }

    @PostMapping("/signingContract")
    @ApiOperation(value = "销售人员与客户签订合同接口",httpMethod = "POST",consumes = "application/json")
    public SystemMsgJsonResponse signingContract(@RequestBody @Validated SigningContractDTO dto,
                                                 Authentication authentication){
        contractService.signingContract(dto,authentication.getName());
        return SystemMsgJsonResponse.success();
    }

    @GetMapping("/getContractCommodity/{id}")
    @ApiOperation(value = "根据合同id查询合同信息",httpMethod = "GET",
            consumes = "application/json",response = ContractVO.class)
    public SystemMsgJsonResponse getContractCommodity(@PathVariable("id") @NotNull(message = "合同id不能为空") Long id){
        return SystemMsgJsonResponse.success(contractService.getContract(id));
    }

    @PostMapping("/getAllContract")
    @ApiOperation(value = "分页查询该账号下所有合同",httpMethod = "POST",
            consumes = "application/json",response = ContractVO.class)
    public SystemMsgJsonResponse getAllContract(@RequestBody @Validated PageContractDTO dto,
                                                Authentication authentication){
        return SystemMsgJsonResponse
                .success(contractService.getPageAllContract(dto,authentication.getName()));
    }


    @PostMapping("/updateContract")
    @ApiOperation(value = "修改合同",httpMethod = "POST",
            consumes = "application/json")
    public SystemMsgJsonResponse updateContract(@RequestBody @Validated UpdateContractDTO dto,
                                                Authentication authentication){
        contractService.updateContract(dto,authentication.getName());
        return SystemMsgJsonResponse.success();
    }

    @PostMapping("/deleteContract/{id}")
    @ApiOperation(value = "删除合同",httpMethod = "POST",consumes = "application/json")
    public SystemMsgJsonResponse deleteContract(@PathVariable("id") @NotNull(message = "合同id不能为空") Long id){
        contractService.deleteContract(id);
        return SystemMsgJsonResponse.success();
    }
}
