package com.siki.provider.controller;

import com.siki.provider.dto.commodity.PageCommodityDTO;
import com.siki.salessystemcommon.utils.SystemMsgJsonResponse;
import com.siki.provider.dto.commodity.AddCommodityDTO;
import com.siki.provider.service.CommodityService;
import com.siki.provider.vo.commodity.CommodityVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
/**
 * @Author Siki
 * @Date 2020/12/16
 */
@RestController
@Api(tags = "商品相关操作模块")
@RequestMapping("/api/vi/siki/commodity")
@CrossOrigin
public class CommodityController {
    private CommodityService commodityService;

    public CommodityController(CommodityService commodityService) {
        this.commodityService = commodityService;
    }

    @PreAuthorize("hasRole('ROLE_PRO_ADMIN')")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ApiOperation(value = "增加商品接口", httpMethod = "POST",
            consumes = "application/json")
    public SystemMsgJsonResponse addCommodity(@RequestBody @Validated AddCommodityDTO dto) {
        commodityService.addCommodity(dto);
        return SystemMsgJsonResponse.success();
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
    @ApiOperation(value = "删除商品接口", consumes = "application/json", httpMethod = "POST")
    public SystemMsgJsonResponse deleteCommodity(@NotNull @PathVariable("id") Long id) {
        commodityService.deleteCommodity(id);
        return SystemMsgJsonResponse.success();
    }

    @RequestMapping(value = "/getCommodity/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "根据id获取一个商品信息", consumes = "application/json",
            httpMethod = "GET",response = CommodityVO.class)
    public SystemMsgJsonResponse getCommodity(@NotNull @PathVariable("id") Long id){
        return SystemMsgJsonResponse.success(commodityService.getCommodity(id));
    }

    @RequestMapping(value = "/page", method = RequestMethod.POST)
    @ApiOperation(value = "根据条件查询商品信息", consumes = "application/json",
            httpMethod = "POST",response = CommodityVO.class)
    public SystemMsgJsonResponse page(@RequestBody @Validated PageCommodityDTO dto){
        return SystemMsgJsonResponse.success(commodityService.page(dto));
    }

    @GetMapping("/current")
    public SystemMsgJsonResponse getCurrent(Authentication authentication){
        System.out.println("------123-----");
        return SystemMsgJsonResponse.success(authentication.getName());
    }
}
