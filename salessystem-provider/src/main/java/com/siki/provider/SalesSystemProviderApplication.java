package com.siki.provider;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
/**
 * @author Siki
 * @date 2021/1/7
 */
@SpringBootApplication
@MapperScan(basePackages = "com.siki.salessystemcommon.mapper")
@EnableDiscoveryClient
// spring security注解，代表使用表达式时间方法级别的安全性 4个注解可用
@EnableGlobalMethodSecurity(prePostEnabled = true)
@ComponentScan(basePackages = {"com.siki.salessystemcommon", "com.siki.provider"})
public class SalesSystemProviderApplication {
	public static void main(String[] args) {
		SpringApplication.run(SalesSystemProviderApplication.class, args);
	}
}
