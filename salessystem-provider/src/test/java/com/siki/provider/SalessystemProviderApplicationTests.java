package com.siki.provider;


import org.springframework.boot.test.context.SpringBootTest;

import java.util.Scanner;

@SpringBootTest
class SalessystemProviderApplicationTests {
    public static void main(String[] args) {
//        System.out.println(isSubsequence("xihui", "xiqhu"));

    }


    public int waysToStep(int n,int k) {
        if (n <= 2) {
            return n;
        }
        if (n == 3) {
            return 4;
        }

        Node node = new Node(7);
        node.next = new Node(1);
        node.next.next = new Node(2);
        node.next.next.next = new Node(4);
        node.next.next.next.next = node;
        for (int i = 4; i < n + 1; i++) {
            node.var = (int) (((long) node.next.var + node.next.next.var + node.next.next.next.var) % 1000000007);
            node = node.next;
        }
        return node.next.next.next.var;
    }

    static class Node {
        int var;
        Node next;

        Node(int i) {
            var = i;
        }
    }

}
