# salessystem

#### 介绍
分布式 RPC公司销售管理系统

近些年分布式框架越来越火，作为有志冲击带厂（不是）的带学生，怎么说也要来学习一下。
<br/> 学习后正愁没有可以实践的地方，正好学校期末作业是公司的销售系统，思考了一下觉得可以用来实践一下
<br/> 于是该项目营运而生

#### 软件架构
Spring-Cloud RPC框架
- 应用Spring Cloud Eureka作为服务注册中心
- 应用Spring Cloud Zuul作为网关分发请求
- 应用MyBatis-Plus作为持久层框架
- 使用Ribbon实现了负载均衡技术，自定义均衡算法
- 拆分Spring Security成单独微服务作为权限验证中心
- 拆分多个数据库实现读写分离
- 使用Redis实现分布式锁技术


#### 安装教程

push后在本地启动即可，网关端口为4396

#### 参与贡献

独立完成

#### 项目心得
项目主要是用于实践RPC框架和负载均衡算法的实现，为能够对RPC这一块了解的深入一点。  
本来是想把整个Spring Cloud的常用组件都使用一遍，至少体会一下怎么实现的  
可惜项目大小有限，像hystrix这些就很遗憾没有用上，希望以后能够接触到更好的项目。
