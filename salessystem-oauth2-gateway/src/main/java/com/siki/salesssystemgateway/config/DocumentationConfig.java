package com.siki.salesssystemgateway.config;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;

import java.util.ArrayList;
import java.util.List;

/**
 * 资源文档配置类
 *
 * @author siki
 * @date 2021/12/24
 */
@Component
@Primary
public class DocumentationConfig implements SwaggerResourcesProvider {

    @Override
    public List<SwaggerResource> get() {
        List<SwaggerResource> resources = new ArrayList<>();
        /*
            resources.add(swaggerResource("消费者相关接口","/consumer/v2/api-docs","1.0"));
            /api/search/是网关路由，/v2/api-docs是swagger中的
         */
        resources.add(swaggerResource("授权中心微服务", "/auth/v2/api-docs", "1.0"));
        resources.add(swaggerResource("销售管理后台接口微服务", "/provider/v2/api-docs", "1.0"));
        return resources;
    }

    private SwaggerResource swaggerResource(String name, String location, String version) {
        SwaggerResource swaggerResource = new SwaggerResource();
        swaggerResource.setName(name);
        swaggerResource.setLocation(location);
        swaggerResource.setSwaggerVersion(version);
        return swaggerResource;
    }

}