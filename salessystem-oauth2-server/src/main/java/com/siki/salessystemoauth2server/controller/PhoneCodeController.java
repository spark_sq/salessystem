package com.siki.salessystemoauth2server.controller;

import com.aliyuncs.utils.StringUtils;
import com.siki.salessystemcommon.entity.enumeration.GlobalServiceMsgCode;
import com.siki.salessystemcommon.utils.SystemMsgJsonResponse;
import com.siki.salessystemoauth2server.bean.SmsProperties;
import com.siki.salessystemoauth2server.service.RedisService;
import com.siki.salessystemoauth2server.util.phoneverification.VerificationCodeUtil;
import com.siki.salessystemoauth2server.util.phoneverification.SmsTool;
import com.tencentcloudapi.sms.v20210111.models.SendStatus;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.Map;

/**
 * @author keyvonchen
 */
@RestController
@Api(tags = "[短信验证码相关操作模块]")
@RequestMapping("/sms")
public class PhoneCodeController {

    private final SmsProperties smsProperties;
    /***
     * 注入redis模版
     */
    @Autowired
    private RedisService redisService;

    public PhoneCodeController(SmsProperties smsProperties) {
        this.smsProperties = smsProperties;
    }

    /**
     * 发送短信
     */
    @ResponseBody
    @RequestMapping(value = "/smsXxs", method = RequestMethod.POST, headers = "Accept=application/json")
    public SystemMsgJsonResponse smsXxs(String number){
        String[] phones = {"86" + number};
        // 调用工具栏中生成验证码方法（指定长度的随机数）
        String code = VerificationCodeUtil.createVerifyCode(6);
        //填充验证码
        String[] templateParam = {code};
        //传入手机号码及短信模板中的验证码占位符
        SendStatus sms = SmsTool.sendSms(smsProperties, phones, templateParam);
        String signKey = VerificationCodeUtil.createSmsCacheKey("phone_code", number, "sign");
        assert sms != null;
        if ("Ok".equals(sms.getCode())) {
            redisService.setExpire(signKey, code, Long.parseLong("5"));
            return SystemMsgJsonResponse.success();
        } else {
            return SystemMsgJsonResponse.fail(sms.getMessage());
        }
    }


    /**
     * 注册验证
     */
    @ResponseBody
    @RequestMapping(value = "/validateNum", method = RequestMethod.POST, headers = "Accept=application/json")
    public SystemMsgJsonResponse validateNum(@RequestBody Map<String, Object> requestMap) {

        //获取注册手机号码
        String phone = requestMap.get("phone").toString();
        //获取手机验证码
        String verifyCode = requestMap.get("verifyCode").toString();
        //首先比对验证码是否失效
        String signKey = VerificationCodeUtil.createSmsCacheKey("phone_code", phone, "sign");
        String authCode = redisService.get(signKey);
        if (!StringUtils.isEmpty(authCode)) {
            if ("".equals(authCode) || verifyCode.equals(authCode)) {
                //用户注册成功
                return SystemMsgJsonResponse.success();
            } else {
                //验证码错误
                return SystemMsgJsonResponse.fail(GlobalServiceMsgCode.USER_NO_PHONE_CODE);
            }
        } else {
            //验证码错误
            return SystemMsgJsonResponse.fail(GlobalServiceMsgCode.USER_NO_PHONE_CODE);
        }
    }

}
