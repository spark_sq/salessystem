package com.siki.salessystemoauth2server.controller;


import com.siki.salessystemcommon.utils.SystemMsgJsonResponse;
import com.siki.salessystemoauth2server.dto.RegisteredAccountDTO;
import com.siki.salessystemoauth2server.service.AccountService;
import io.swagger.annotations.Api;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
/**
 * @author Siki
 * @date 2020/12/27
 */
@RestController
@Api(tags = "[账号相关操作模块[注册/删除]]")
@RequestMapping("/account")
public class AccountController {
    private AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping(value = "/registered")
    public SystemMsgJsonResponse registered(@RequestBody @Validated RegisteredAccountDTO dto) throws Exception {
        accountService.registered(dto);
        return SystemMsgJsonResponse.success();
    }

    @PostMapping(value = "/delete/{id}")
    public SystemMsgJsonResponse deleteAccount(@PathVariable("id") @NotNull(message = "账号id不能为空") String id){
        accountService.deleteAccount(id);
        return SystemMsgJsonResponse.success();
    }
}
