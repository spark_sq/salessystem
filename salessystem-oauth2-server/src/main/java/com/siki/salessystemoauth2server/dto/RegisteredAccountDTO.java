package com.siki.salessystemoauth2server.dto;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * @Author Siki
 * @Date 2020/12/4
 */
@ApiModel("账号注册DTO")
@Getter
@Setter
@NoArgsConstructor
public class RegisteredAccountDTO {
    @NotNull(message = "账号不能为空")
    @ApiModelProperty("账号")
    String account;

    @NotNull(message = "密码不能为空")
    @ApiModelProperty("密码")
    String password;

    @ApiModelProperty("账户类型")
    String accountType;
}
