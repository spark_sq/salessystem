package com.siki.salessystemoauth2server.service;

import com.siki.salessystemoauth2server.dto.RegisteredAccountDTO;

public interface AccountService {
    /**
     * 注册
     *
     * @param dto 注册请求体
     */
    void registered(RegisteredAccountDTO dto) throws Exception;

    /**
     * 删除账号
     * @param id 账号id
     */
    void deleteAccount(String id);
}
