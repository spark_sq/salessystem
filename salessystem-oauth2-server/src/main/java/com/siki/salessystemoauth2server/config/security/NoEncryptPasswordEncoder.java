package com.siki.salessystemoauth2server.config.security;

import com.siki.salessystemoauth2server.util.DigestUtil;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;


/**
 * 自定义加密策略
 *
 * @Author Siki
 * @Date 2020/12/5
 */
public class NoEncryptPasswordEncoder implements PasswordEncoder {

    @Override
    public String encode(CharSequence charSequence) {
        return (String) charSequence;
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        if (rawPassword == null || encodedPassword == null) {
            return false;
        } else {
            try {
                return bCryptPasswordEncoder.matches(DigestUtil
                                .encrypt((String) rawPassword),
                        encodedPassword);
            } catch (Exception e) {
                return false;
            }
        }

    }
}