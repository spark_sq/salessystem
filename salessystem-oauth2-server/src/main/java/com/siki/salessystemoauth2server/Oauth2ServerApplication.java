

package com.siki.salessystemoauth2server;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
/**
 * @Author Siki
 * @Date 2020/12/2
 */
@SpringBootApplication
@EnableResourceServer
@EnableEurekaClient
@MapperScan(basePackages = "com.siki.salessystemcommon")
//@ComponentScan(basePackages = {"com.siki.salessystemcommon.mapper","com.siki.salessystemoauth2server"})
public class Oauth2ServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(Oauth2ServerApplication.class, args);
    }
}
